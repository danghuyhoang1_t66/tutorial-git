async function logUser() {
    try {
        const response = await fetch("http://localhost:8080/users/5");
        if (!response.ok) {
            throw new Error(`HTTP error! status: ${response.status}`);
        }
        const data = await response.json();
        alert(`ID: ${data.id}\nName: ${data.name}\nBirthday: ${data.birthDay}\nEmail: ${data.email}\nPhone: ${data.phone}\nDescription: ${data.description}`);
    } catch (error) {
        console.error('There was a problem with the fetch operation:', error);
    }
}

logUser().then(() => {
    console.log('User data fetched successfully');
}).catch(error => {
    console.error('Error in logUser:', error);
});
